<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   22.05.2019
 */

namespace alexs\yii2lists\actions\traits;
use alexs\yii2lists\models\AbstractFilterListModel;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * Trait TraitFilterListAction
 * @package alexs\yii2lists\actions\traits
 * @property string $layout
 * @property string $view
 * @method Pagination paginate(ActiveQuery $ActiveQuery)
 * @method mixed displayView(string $layout, string $view, array $params)
 */

trait TraitFilterListAction
{
    /**
     * @return ActiveQuery
     */
    abstract public function findItems();

    /**
     * @return AbstractFilterListModel
     */
    abstract public function getFilterListModel();

    /**
     * @return string|null
     */
    public function run() {
        $ActiveQuery = $this->findItems();
        $FilterListModel = $this->filterList($ActiveQuery);
        $Pagination = $this->paginate($ActiveQuery);
        $models = $ActiveQuery->all();
        return $this->displayView($this->layout, $this->view, [
            'FilterListModel'=>$FilterListModel,
            'models'=>$models,
            'Pagination'=>$Pagination,
        ]);
    }

    /**
     * @param ActiveQuery $ActiveQuery
     * @return AbstractFilterListModel
     */
    protected function filterList(ActiveQuery $ActiveQuery) {
        $FilterListModel = $this->getFilterListModel();
        if ($FilterListModel->load($this->getFilteredData()) && $FilterListModel->validate()) {
            $FilterListModel->filter($ActiveQuery);
        }
        return $FilterListModel;
    }

    /**
     * @return array|mixed
     */
    protected function getFilteredData() {
        return \Yii::$app->request->get();
    }
}
