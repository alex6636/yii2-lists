<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   23.05.2019
 */

namespace alexs\yii2lists\actions\traits;

trait TraitFilterUpdateListAction
{
    use TraitFilterListAction, TraitUpdateListAction;

    /**
     * @return string|null
     */
    public function run() {
        $ActiveQuery = $this->findItems();
        $FilterListModel = $this->filterList($ActiveQuery);
        $Pagination = $this->paginate($ActiveQuery);
        $models = $ActiveQuery->all();
        if ($this->updateList($models)) {
            if ($Response = $this->afterUpdateList($models)) {
                return $Response;
            }
        }
        return $this->displayView($this->layout, $this->view, [
            'FilterListModel'=>$FilterListModel,
            'models'=>$models,
            'Pagination'=>$Pagination,
        ]);
    }
}
