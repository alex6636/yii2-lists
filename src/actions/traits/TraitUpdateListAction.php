<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   26.05.2019
 */

namespace alexs\yii2lists\actions\traits;
use alexs\yii2crud\actions\traits\TraitMessageable;
use yii\data\Pagination;
use yii\db\ActiveRecord;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\web\Controller;
use yii\base\Response;

/**
 * Trait TraitFilterListAction
 * @package alexs\yii2lists\actions\traits
 * @property string $layout
 * @property string $view
 * @property Controller $controller
 * @method Pagination paginate(ActiveQuery $ActiveQuery)
 * @method mixed displayView(string $layout, string $view, array $params)
 */

trait TraitUpdateListAction
{
    use TraitMessageable;
    
    /**
     * @return ActiveQuery
     */
    abstract public function findItems();

    /**
     * @return null|string
     */
    public function scenarioUpdateList() {
        return null;
    }
    
    /**
     * @return string|null
     */
    public function run() {
        $ActiveQuery = $this->findItems();
        $Pagination = $this->paginate($ActiveQuery);
        $models = $ActiveQuery->all();
        if ($this->updateList($models)) {
            if ($Response = $this->afterUpdateList($models)) {
                return $Response;
            }
        }
        return $this->displayView($this->layout, $this->view, [
            'models'=>$models,
            'Pagination'=>$Pagination,
        ]);
    }

    /**
     * @param ActiveRecord[] $models
     * @return bool
     */
    protected function updateList($models) {
        if (!$data = $this->getUpdatedData()) {
            return false;
        }
        if ($scenario = $this->scenarioUpdateList()) {
            $this->setMultipleScenario($models, $scenario);
        }
        if (Model::loadMultiple($models, $data) && Model::validateMultiple($models)) {
            foreach ($models as $model) {
                $model->save(false);
            }
            $this->displayMessage($this->getSuccessMessageText());
            return true;
        } else {
            $this->displayMessage($this->getErrorMessageText(), 'error');
        }
        return false;
    }

    /**
     * @param ActiveRecord[] $models
     * @param string $scenario
     */
    protected function setMultipleScenario($models, $scenario) {
        foreach ($models as $Model) {
            $Model->setScenario($scenario);
        }
    }
    
    /**
     * @return array|mixed
     */
    protected function getUpdatedData() {
        return \Yii::$app->request->post();
    }

    /**
     * @param ActiveRecord[] $models
     * @return Response|null
     */
    protected function afterUpdateList($models) {
        return $this->controller->refresh();
    }

    /**
     * @return string
     */
    protected function getSuccessMessageText() {
        return \Yii::t('app', 'Data has been successfully updated');
    }

    /**
     * @return string
     */
    protected function getErrorMessageText() {
        return \Yii::t('app', 'Failed to update data');
    }
}
