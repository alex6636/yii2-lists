<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   22.05.2019
 */

namespace alexs\yii2lists\models;
use yii\base\Model;
use yii\db\ActiveQuery;

abstract class AbstractFilterListModel extends Model
{
    /**
     * @param ActiveQuery $ActiveQuery
     */
    abstract public function filter(ActiveQuery $ActiveQuery);
}
