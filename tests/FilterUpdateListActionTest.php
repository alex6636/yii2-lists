<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   23.05.2019
 */

namespace alexs\yii2lists\tests;
use alexs\yii2crud\tests\models\Article;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;
use Yii;

class FilterUpdateListActionTest extends DatabaseTableTestCase
{
    public function testIndexAction() {
        for ($i = 1; $i <= 3; $i ++) {
            $data = [
                'id'   =>$i,
                'title'=>'Article ' . $i,
                'text' =>'Article contents ' . $i,
            ];
            $Article = new Article;
            $Article->setAttributes($data);
            $Article->save();
        }
        Yii::$app->request->setQueryParams([
            'FilterListModel'=>['title'=>'Article 2'],
        ]);
        Yii::$app->request->setBodyParams([
            'Article'=>[
                0=>[
                    'title'=>'Article 2 edited',
                    'text'=>'Article contents 2 edited',
                ],
            ],
        ]);
        $result = Yii::$app->runAction('/article/index-filter-update-list');
        $this->assertInstanceOf('alexs\yii2lists\tests\models\article\FilterListModel', $result['FilterListModel']);
        $this->assertInstanceOf('alexs\yii2crud\models\CrudModel', $result['models'][0]);
        $this->assertCount(1, $result['models']);
        $this->assertSame('Article 2 edited', $result['models'][0]->title);
        $this->assertInstanceOf('yii\data\Pagination', $result['Pagination']);
    }

    protected function setUp() {
        parent::setUp();
        Yii::$app->controllerNamespace = 'alexs\\yii2lists\\tests\\controllers';
    }

    protected function getTableName() {
        return 'article';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
        ];
    }
}
