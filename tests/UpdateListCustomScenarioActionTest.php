<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   23.05.2019
 */

namespace alexs\yii2lists\tests;
use alexs\yii2crud\tests\models\Article;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;
use Yii;

class UpdateListCustomScenarioActionTest extends DatabaseTableTestCase
{
    public function testIndexAction() {
        for ($i = 1; $i <= 3; $i ++) {
            $data = [
                'id'   =>$i,
                'title'=>'Article ' . $i,
                'text' =>'Article contents ' . $i,
            ];
            $Article = new Article;
            $Article->setAttributes($data);
            $Article->save();
        }
        // updating
        Yii::$app->request->setBodyParams([
            'Article'=>[
                1=>[
                    'title'=>'Article 2 edited',
                    'text'=>'Article contents 2 edited',
                ],
            ],
        ]);
        $result = Yii::$app->runAction('/article/index-update-list-custom-scenario');
        $this->assertSame('Article 2 edited', $result['models'][1]->title);
        $this->assertSame('Article contents 2', $result['models'][1]->text);
    }

    protected function setUp() {
        parent::setUp();
        Yii::$app->controllerNamespace = 'alexs\\yii2lists\\tests\\controllers';
    }

    protected function getTableName() {
        return 'article';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
        ];
    }
}
