<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   22.05.2019
 */

namespace alexs\yii2lists\tests\actions\article;
use alexs\yii2crud\actions\IndexAction;
use alexs\yii2lists\actions\traits\TraitFilterListAction;
use alexs\yii2lists\tests\models\article\Article;
use alexs\yii2lists\tests\models\article\FilterListModel;
use yii\db\ActiveQuery;

class IndexFilterListAction extends IndexAction
{
    use TraitFilterListAction;

    /**
     * @return ActiveQuery
     */
    public function findItems() {
        return Article::find()->orderBy('title');
    }

    /**
     * @return FilterListModel
     */
    public function getFilterListModel() {
        return new FilterListModel;
    }

    /**
     * @param string $layout
     * @param string $view
     * @param array $params
     * @return mixed
     */
    protected function displayView($layout, $view, $params) {
        return $params;
    }
}
