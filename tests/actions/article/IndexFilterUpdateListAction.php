<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   23.05.2019
 */

namespace alexs\yii2lists\tests\actions\article;
use alexs\yii2crud\actions\IndexAction;
use alexs\yii2lists\actions\traits\TraitFilterUpdateListAction;
use alexs\yii2lists\tests\models\article\Article;
use alexs\yii2lists\tests\models\article\FilterListModel;
use yii\base\Response;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class IndexFilterUpdateListAction extends IndexAction
{
    use TraitFilterUpdateListAction;

    /**
     * @return ActiveQuery
     */
    public function findItems() {
        return Article::find()->orderBy('title');
    }

    /**
     * @return FilterListModel
     */
    public function getFilterListModel() {
        return new FilterListModel;
    }

    /**
     * @param ActiveRecord[] $models
     * @return Response|null
     */
    protected function afterUpdateList($models) {
        // do nothing
        return null;
    }

    /**
     * Display the message
     * 
     * @param string $message
     * @param string $key
     */
    protected function displayMessage($message, $key = 'message') {
        // do nothing
    }

    /**
     * @param string $layout
     * @param string $view
     * @param array $params
     * @return mixed
     */
    protected function displayView($layout, $view, $params) {
        return $params;
    }
}
