<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   22.05.2019
 */

namespace alexs\yii2lists\tests\actions\article;
use alexs\yii2crud\actions\IndexAction;
use alexs\yii2lists\actions\traits\TraitUpdateListAction;
use alexs\yii2lists\tests\models\article\Article;
use yii\base\Response;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class IndexUpdateListAction extends IndexAction
{
    use TraitUpdateListAction;

    /**
     * @return ActiveQuery
     */
    public function findItems() {
        return Article::find()->orderBy('title');
    }

    /**
     * @param ActiveRecord[] $models
     * @return Response|null
     */
    protected function afterUpdateList($models) {
        // do nothing
        return null;
    }

    /**
     * Display the message
     * 
     * @param string $message
     * @param string $key
     */
    protected function displayMessage($message, $key = 'message') {
        // do nothing
    }

    /**
     * @param string $layout
     * @param string $view
     * @param array $params
     * @return mixed
     */
    protected function displayView($layout, $view, $params) {
        return $params;
    }
}
