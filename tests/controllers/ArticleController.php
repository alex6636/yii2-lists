<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   22.05.2019
 */

namespace alexs\yii2lists\tests\controllers;
use alexs\yii2crud\controllers\CrudController;

class ArticleController extends CrudController
{
    public $actions_namespace = '\alexs\yii2lists\tests\actions';
    public $model_namespace = '\alexs\yii2lists\tests\models';

    public function actions() {
        return array_merge(parent::actions(), [
            'index-filter-list'=>'alexs\yii2lists\tests\actions\article\IndexFilterListAction',
            'index-update-list'=>'alexs\yii2lists\tests\actions\article\IndexUpdateListAction',
            'index-filter-update-list'=>'alexs\yii2lists\tests\actions\article\IndexFilterUpdateListAction',
            'index-update-list-custom-scenario'=>'alexs\yii2lists\tests\actions\article\IndexUpdateListCustomScenarioAction',
        ]);
    }
}
