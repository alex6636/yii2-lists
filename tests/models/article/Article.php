<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   22.05.2019
 */

namespace alexs\yii2lists\tests\models\article;
use alexs\yii2crud\models\CrudModel;

/**
 * @property int $id
 * @property string $title
 * @property string $text
 */

class Article extends CrudModel
{
    const SCENARIO_CUSTOM = 'custom';
    
    public function rules() {
        return [
            [['title', 'text'], 'filter', 'filter'=>'trim'],
            [['id', 'title'], 'required'],
            ['id', 'integer'],
        ];
    }
    
    public function scenarios() {
        return array_merge(parent::scenarios(), [
             static::SCENARIO_CUSTOM=>['title'], // just update a title
        ]);
    }
}
