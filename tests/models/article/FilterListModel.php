<?php
namespace alexs\yii2lists\tests\models\article;
use alexs\yii2lists\models\AbstractFilterListModel;
use yii\db\ActiveQuery;

class FilterListModel extends AbstractFilterListModel
{
    public $title;
    public $text;

    /**
     * @param ActiveQuery $ActiveQuery
     */
    public function filter(ActiveQuery $ActiveQuery) {
        if ($this->title) {
            $ActiveQuery->andWhere(['title'=>$this->title]);
        }
        if ($this->text) {
            $ActiveQuery->andWhere(['text'=>$this->text]);
        }
    }

    public function rules() {
        return [
            [['title', 'text'], 'filter', 'filter'=>'trim'],
        ];
    }
}
